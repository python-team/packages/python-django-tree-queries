Source: python-django-tree-queries
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-babel,
 python3-django,
 python3-hatchling,
 python3-pytest,
 python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.7.0
Homepage: https://github.com/feincms/django-tree-queries
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-tree-queries
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-tree-queries.git
Rules-Requires-Root: no

Package: python-django-tree-queries-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Adjacency-list trees for Django (Documentation)
 Query Django model trees using adjacency lists and recursive common table
 expressions. Supports PostgreSQL, sqlite3 and MariaDB and MySQL.
 .
 Features and limitations:
  * Supports only integer and UUID primary keys (for now).
  * Allows specifying ordering among siblings.
  * Uses the correct definition of depth, where root nodes have a depth of
    zero.
  * The parent foreign key must be named "parent" at the moment.
  * The fields added by the common table expression always are tree_depth,
    tree_path and tree_ordering. The names cannot be changed. tree_depth is an
    integer, tree_path an array of primary keys and tree_ordering an array of
    values used for ordering nodes within their siblings.
  * Besides adding the fields mentioned above the package only adds queryset
    methods for ordering siblings and filtering ancestors and descendants.
  * Little code, and relatively simple when compared to other tree management
    solutions for Django. No redundant values so the only way to end up with
    corrupt data is by introducing a loop in the tree structure (making it a
    graph). The TreeNode abstract model class has some protection against this.
  * Supports only trees with max. 50 levels on MySQL/MariaDB, since those
    databases do not support arrays and require a maximum length for the
    tree_path and tree_ordering upfront.
 .
 This package contains the documentation.

Package: python3-django-tree-queries
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-tree-queries-doc,
Description: Adjacency-list trees for Django (Python3 version)
 Query Django model trees using adjacency lists and recursive common table
 expressions. Supports PostgreSQL, sqlite3 and MariaDB and MySQL.
 .
 Features and limitations:
  * Supports only integer and UUID primary keys (for now).
  * Allows specifying ordering among siblings.
  * Uses the correct definition of depth, where root nodes have a depth of
    zero.
  * The parent foreign key must be named "parent" at the moment.
  * The fields added by the common table expression always are tree_depth,
    tree_path and tree_ordering. The names cannot be changed. tree_depth is an
    integer, tree_path an array of primary keys and tree_ordering an array of
    values used for ordering nodes within their siblings.
  * Besides adding the fields mentioned above the package only adds queryset
    methods for ordering siblings and filtering ancestors and descendants.
  * Little code, and relatively simple when compared to other tree management
    solutions for Django. No redundant values so the only way to end up with
    corrupt data is by introducing a loop in the tree structure (making it a
    graph). The TreeNode abstract model class has some protection against this.
  * Supports only trees with max. 50 levels on MySQL/MariaDB, since those
    databases do not support arrays and require a maximum length for the
    tree_path and tree_ordering upfront.
 .
 This package contains the Python 3 version of the library.
